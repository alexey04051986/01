package omazuryk.automation.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DataGenerator {

	public int maxSize;
	public int values;

	// Array List Generator
	public static List<Integer> randomArrayList(int maxSize, int values) {

		int nextInt = new Random().nextInt(maxSize - 1) + 1;

		List<Integer> list = new ArrayList<Integer>();

		Random randomGenerator = new Random();

		for (int i = 0; i < nextInt; i++) {
			int randomelement = randomGenerator.nextInt(values);
			list.add(randomelement);
		}
		return list;
	}
}
