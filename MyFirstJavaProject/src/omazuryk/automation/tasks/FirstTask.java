package omazuryk.automation.tasks;

import java.util.List;
//import omazuryk.automation.tasks.*;

public class FirstTask {
	// � ������ �������� ������ ������� �� ��������� ������� �������� �1 ��
	// ����-���� ����� ������� �2;

	public static List<Integer> list(int maxSize, int values) {
		
		System.out.println("\nFirst Task /-----------------------------");

		List<Integer> list = DataGenerator.randomArrayList(maxSize, values); // calling generator for initial data

		System.out.println("\nInitial List: " + list);

		for (int i = 0; i < list.size() / 2; i++) {
			int current = list.get(i);
			if (current == 0) {
				list.set(i, 1);
			}
		}
		System.out.println("Modified Array_List(replaced '0' with '1'): " + list);
		System.out.println("\nEnd of First Task /-----------------------------");
		return list;
		
	}

}
